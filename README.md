## Description

This is a very basic music collection app. It's built on top of these frameworks:

* Laravel 5.7
* VueJs 2 (with Vue Router for better user experience)

A working copy of the project [can be found here](https://music-collection-app.danielcintra.site)

The login credentials are:

* **User:** madesimple
* **Password:** secret

## Project Overview

The project is structured in a modular concept. In `./modules` you will find these directories:

#### Support

Base classes (ServiceProviders, Controllers, Models) that generally, the other modules (like **Artist** and **Album**) will extend.

#### Login

A simple **Login** module that can be easily adapted from one project to another (password resets, not included in this example). This module can be duplicated and used in apps that requires multiple login systems, like an **Admin Panel** and a **Customers Restrict Area**.

#### Adm

Once logged in, the `Vue Router` assumes the app `view/routes mapping`. The `Adm` module "prepares the stage" to View Router, with a Laravel catch all route (`./modules/Adm/routes/web.php`), and a Laravel Blade View ((`./modules/Adm/views/index.blade.php`)) that hosts the `master` Vue Component.

The Vue Components are also modular, and are stored in `./resources/backend/js/modules`.

With the `master` Vue Component, we have a side menu, and we can start to navigate to the necessary app view components (the app's Vue modules).

#### Artist

The Laravel API endpoints to Artist CRUD operations.

#### Album

The Laravel API endpoints to Album CRUD operations.

## Project Install

I recommend [Laravel Homestead](https://laravel.com/docs/5.7/homestead) (pre-packaged [Vagrant](https://www.vagrantup.com/) box) as the environment to test this kind of app, but you can install it in any environment that can run a Laravel 5.7 app.

To install this project, clone this repository:

```
git clone git@gitlab.com:daniel-cintra/music-collection-app.git
```

Cd into your project folder and install the composer packages:

```
composer install
```

Install the npm packages:

```
yarn install
```

Rename the **.env.example** file stored in the project root dir, to **.env**. Open this file (**.env**) in your editor of choice and update the database credentials/info as needed. The project is using a MySQL database by default.

Generate the project encryption key:

```
php artisan key:generate
```

If you are using [Laravel Homestead](https://laravel.com/docs/5.7/homestead), ssh into your VM:

```
cd ~/Homestead && vagrant ssh
```

Navigate to your project dir and migrate the database:

```
cd ~/your-document-root-dir/your-project-path
php artisan migrate
``` 

And run the database seeder, it will populate the database for you:

```
php artisan db:seed
```

## Compiling Assets

The project uses [Laravel Mix](https://laravel.com/docs/5.7/mix) to compile the assets (.scss and .js files).

Check the **./webpack.mix.js** configuration file changing the proxy hostname at the end of the file as needed.

To compile the files in dev mode, cd into the project dir and run:

```
npm run watch
```

To compile the files for production, cd into the project dir and run:

```
npm run prod
```