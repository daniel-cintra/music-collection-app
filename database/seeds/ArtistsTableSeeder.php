<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ArtistsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $artists = $this->getArtists();
        
        DB::table('artists')->insert($artists);
    }

    private function getArtists()
    {
        
        $now = Carbon::now();
        
        return [

            ['name' => 'Ben Harper', 'twitter_handle' => '@BenHarper', 'created_at' => $now, 'updated_at' => $now],

            ['name' => 'Bob Marley', 'twitter_handle' => '@bobmarley', 'created_at' => $now, 'updated_at' => $now],

            ['name' => 'Jeff Beck', 'twitter_handle' => '@jeffbeckmusic', 'created_at' => $now, 'updated_at' => $now],

            ['name' => 'Led Zeppelin', 'twitter_handle' => '@ledzeppelin', 'created_at' => $now, 'updated_at' => $now],

            ['name' => 'Pink Floyd', 'twitter_handle' => '@pinkfloyd', 'created_at' => $now, 'updated_at' => $now],

            ['name' => 'The Rolling Stones', 'twitter_handle' => '@RollingStones', 'created_at' => $now, 'updated_at' => $now],

        ];
    }

}
