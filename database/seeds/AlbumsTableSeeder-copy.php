<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AlbumsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $albums = $this->getAlbums();
        
        DB::table('albums')->insert($albums);
    }

    private function getAlbums()
    {
        
        $now = Carbon::now();
        
        return [

            ['artist_id' => 1, 'name' => 'Diamonds on the Inside', 'year' => 2003, 'created_at' => $now, 'updated_at' => $now],

            ['artist_id' => 1, 'name' => 'There Will Be a Light', 'year' => 2004, 'created_at' => $now, 'updated_at' => $now],

            ['artist_id' => 1, 'name' => 'Get Up!', 'year' => 2013, 'created_at' => $now, 'updated_at' => $now],


            ['artist_id' => 2, 'name' => 'Soul Rebels', 'year' => 1970, 'created_at' => $now, 'updated_at' => $now],

            ['artist_id' => 2, 'name' => 'Babylon by Bus', 'year' => 1978, 'created_at' => $now, 'updated_at' => $now],

            ['artist_id' => 2, 'name' => 'Uprising', 'year' => 1980, 'created_at' => $now, 'updated_at' => $now],


            ['artist_id' => 3, 'name' => 'Truth', 'year' => 1968, 'created_at' => $now, 'updated_at' => $now],

            ['artist_id' => 3, 'name' => 'Who Else!', 'year' => 1999, 'created_at' => $now, 'updated_at' => $now],

            ['artist_id' => 3, 'name' => 'Loud Hailer', 'year' => 2010, 'created_at' => $now, 'updated_at' => $now],


            ['artist_id' => 4, 'name' => 'Houses of the Holy', 'year' => 1973, 'created_at' => $now, 'updated_at' => $now],

            ['artist_id' => 4, 'name' => 'In Through the Out Door', 'year' => 1979, 'created_at' => $now, 'updated_at' => $now],

            ['artist_id' => 4, 'name' => 'Celebration Day', 'year' => 2012, 'created_at' => $now, 'updated_at' => $now],


            ['artist_id' => 5, 'name' => 'The Dark Side of the Moon', 'year' => 1973, 'created_at' => $now, 'updated_at' => $now],

            ['artist_id' => 5, 'name' => 'Wish You Were Here', 'year' => 1975, 'created_at' => $now, 'updated_at' => $now],

            ['artist_id' => 5, 'name' => 'The Endless River', 'year' => 2014, 'created_at' => $now, 'updated_at' => $now],


            ['artist_id' => 6, 'name' => 'Let It Bleed', 'year' => 1969, 'created_at' => $now, 'updated_at' => $now],

            ['artist_id' => 6, 'name' => 'Some Girls', 'year' => 1978, 'created_at' => $now, 'updated_at' => $now],

            ['artist_id' => 6, 'name' => 'Shine a Light', 'year' => 2008, 'created_at' => $now, 'updated_at' => $now],

        ];
    }

}
