const mix = require('laravel-mix');

mix.webpackConfig({
  resolve: {
    alias: {
      resources: path.resolve(__dirname, 'resources'),
    },
  },
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// frontend
mix.combine([

    'node_modules/bootstrap/dist/css/bootstrap.min.css',
    'node_modules/@fortawesome/fontawesome-free/css/all.css',

    'node_modules/sweetalert/dist/sweetalert.css',

    ],

    'public/frontend/css/vendor.css'
);

mix.sass('resources/frontend/sass/app.scss', 'public/frontend/css');

mix.js('resources/frontend/js/app.js', 'public/frontend/js')
.sourceMaps()
;

// backend
mix.combine([

  'node_modules/bootstrap/dist/css/bootstrap.min.css',
  'node_modules/@fortawesome/fontawesome-free/css/all.css',

  'node_modules/sweetalert/dist/sweetalert.css',
  'node_modules/nprogress/nprogress.css',

  ],

  'public/backend/css/vendor.css'
);

mix.sass('resources/backend/sass/app.scss', 'public/backend/css');

mix.js('resources/backend/js/app.js', 'public/backend/js')
.sourceMaps()
;

let browserSyncFiles = [
  'public/frontend/css/*.css', 'public/frontend/js/*.js',
  'public/backend/css/*.css', 'public/backend/js/*.js',
];

// running `npm run production` append a unique hash to the filenames of all compiled files ./public/mix-manifest.json
if (mix.inProduction()) {
  mix.version();
}

mix.browserSync({

  // change to your configured dev host name
  proxy: 'music-collection-app.danielcintra.test',

  files: browserSyncFiles,
  notify: false,
  // notify: {
  //   styles: {
  //     top: 'auto',
  //     bottom: '20px',
  //   },
  // },
});

mix.disableNotifications();
