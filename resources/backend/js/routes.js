import VueRouter from 'vue-router';

import Artists from './modules/artists/components/Artists.vue';
import ArtistForm from './modules/artist/components/ArtistForm.vue';

import Albums from './modules/albums/components/Albums.vue';
import AlbumForm from './modules/album/components/AlbumForm.vue';

import Logout from './modules/login/components/Logout.vue';

const routes = [

    // artists routes
    {path: '/artists', component: Artists},
    {path: '/artist/create', component: ArtistForm, name: 'artistCreate'},
    {path: '/artist/:id/edit', component: ArtistForm, name: 'artistEdit'},

    // albums routes
    {path: '/albums', component: Albums},
    {path: '/album/create', component: AlbumForm, name: 'albumCreate'},
    {path: '/album/:id/edit', component: AlbumForm, name: 'albumEdit'},

    {path: '/logout', component: Logout},
];

const scrollBehavior = (to, from, savedPosition) => {
    return {x: 0, y: 0};
  };

export default new VueRouter({
    routes,
    mode: 'history',
    base: '/music-collection/',
    linkActiveClass: 'is-active',

    scrollBehavior,
});
