/**
 * Converte o valor de float para o formato em R$
 *
 * @param  string
 * @return string
 */
Vue.filter('moeda', function(value) {
  let floatValue = parseFloat(value);
  return accounting.formatMoney(floatValue);
});

/**
 * Formata o tipo de cadastro do cliente para exibição na view.
 *
 * @param  string
 * @return string
 */
Vue.filter('tipoCadastro', function(value) {
  return value === 'pessoaFisica' ? 'Pessoa Física' : 'Pessoa Jurídica';
});

/**
 * Formata a exibição de uma string com um Cpf.
 *
 * @param  string
 * @return string
 */
Vue.filter('cpf', function(value) {
  value = value.replace(/\D/g, '');

  value = value.replace(/(\d{3})(\d)/, '$1.$2');

  value = value.replace(/(\d{3})(\d)/, '$1.$2');

  value = value.replace(/(\d{3})(\d{1,2})$/, '$1-$2');

  return value;
});

/**
 * Formata a exibição de uma string com Cnpj.
 *
 * @param  string
 * @return string
 */
Vue.filter('cnpj', function(value) {
  return value.replace(/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/, '$1.$2.$3/$4-$5');
});

/**
 * Formata a exibição de uma string com CEP.
 *
 * @param  string
 * @return string
 */
Vue.filter('cep', function(value) {
  return value.replace(/^(\d{2})(\d{3})(\d{3})/, '$1.$2-$3');
});

/**
 * Formata a exibição de um telefone.
 *
 * @param  string
 * @return string
 */
Vue.filter('telefone', function(value) {
  if (value.length === 10) {
    return value.replace(/^(\d{2})(\d{4})(\d{4})/, '($1) $2-$3');
  } else {
    return value.replace(/^(\d{2})(\d{5})(\d{4})/, '($1) $2-$3');
  }
});

/**
 * Formata a informação de prazo para o frete.
 *
 * @param  string
 * @return string
 */
Vue.filter('prazoFrete', function(value) {
  let integerPrazo = parseInt(value);

  let txtPrazo = (integerPrazo === 1) ? 'dia para entrega' : 'dias para entrega';

  return `${value} ${txtPrazo}`;
});


/**
 * Formata um número com separador de milhar.
 *
 * @param  string
 * @return string
 */
Vue.filter('milhar', function(value) {
  return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
});

/**
 * Trunca uma string para o limite máximo de caracteres definido.
 * @param string
 */
Vue.filter('truncate', function(value, length, ending = '...') {
  if (value.length < length) {
    return value;
  }

  // length = length - 3;

  return value.substring(0, length) + ending;
});
