/**
* First we will load all of this project's JavaScript dependencies which
* includes Vue and other libraries. It is a great starting point when
* building robust, powerful web applications using Vue and Laravel.
*/

require('./bootstrap/bootstrap');

import {i18n} from './bootstrap/init-vue-i18n';
import router from './routes';
import {store} from './store/index';

const app = new Vue({
    i18n,
    router,
    store,
    el: '#app',
});



