
window._ = require('lodash');
window.Popper = require('popper.js').default;

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
  window.$ = window.jQuery = require('jquery');

  require('bootstrap');
} catch (e) {}

window.Vue = require('vue');

import VueRouter from 'vue-router';
Vue.use(VueRouter);

window.bus = new Vue();

require('./init-axios' );
require('./init-nprogress');
// require('./init-vue-i18n');
require('./init-vee-validate');
require('./init-vue-notification');
require('./init-vue-the-mask');
require('./import-libs');
require('../filters');
require('./register-components');
