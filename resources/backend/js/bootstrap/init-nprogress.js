import NProgress from 'nprogress';
NProgress.configure({showSpinner: false});

// Add a request interceptor
window.axios.interceptors.request.use(function(config) {
    // Do something before request is sent
    NProgress.start();

    return config;
  }, function(error) {
    // Do something with request error
    NProgress.done();

    return Promise.reject(error);
  });

// Add a response interceptor
window.axios.interceptors.response.use(function(response) {
    // Do something with response data
    NProgress.done();

    return response;
  }, function(error) {
    NProgress.done();

    if (error.response.status === 401) {
      swal({
        title: 'Ooops...',
        text: 'Sua sessão expirou, por favor faça login novamente',
        icon: 'error',
      })
      .then(() => {
        window.location.href = '/painel/login';
      });
    } else {
      swal('Ooops...', 'Ocorreu um problema :/', 'error');
    }

    return Promise.reject(error);
  });
