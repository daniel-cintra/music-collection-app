import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

// global
import getters from './getters';
import mutations from './mutations';
import actions from './actions';

// modules
import master from '../modules/master/store/index';

export const store = new Vuex.Store({

  state: {},
  getters,
  mutations,
  actions,

  modules: {

    master,

  },

});
