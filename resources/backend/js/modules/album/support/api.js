export default {

  // populates the artists select combo
  fetchArtists(next) {
    axios.get('/api/album/artists')
    .then((response) => {
      next((vm) => {
        vm.artists = response.data;
        vm.artist_id = null;
      });
    });
  },

  storeAlbum(params) {
    axios.post('/api/album', params)
    .then((response) => {
      Vue.nextTick(() => {
        bus.$emit('album/formSubmitted');
      });
    });
  },

  fetchAlbum(to, next) {
    axios.get(`/api/album/${to.params.id}/edit`)
    .then((response) => {
      next((vm) => {
        vm.id = response.data.album.id;
        vm.artist_id = response.data.album.artist_id;
        vm.name = response.data.album.name;
        vm.year = response.data.album.year;

        vm.artists = response.data.artists;
      });
    });
  },

  updateAlbum(vm, params) {
    axios.put(`/api/album/${vm.id}`, params)
    .then((response) => {
      bus.$emit('album/formSubmitted');
    });
  },

};
