export default {

  fetchArtists(next) {
    axios.get('/api/artist')
    .then((response) => {
      next((vm) => {
        // console.log(response.data);
        vm.artists = response.data;
      });
    })
    .catch((error) => {
      next(false);
    });
  },

  deleteArtist(vm, artist) {
    axios.delete(`/api/artist/${artist.id}`)
    .then((response) => {
      if (response.data.status) {
        let index = vm.artists.indexOf(artist);

        vm.artists.splice(index, 1);

        Vue.notify({
          type: 'success',
          text: 'Artist deleted!',
        });
      }
    });
  },

};
