export default {

    sendAuthRequest(vm) {

        vm.btnSubmitText = 'Wait...';

        axios.post('/panel/login', {username: vm.username, password: vm.password})
        .then((response) => {
          // success
          if (response.data.status) {
            window.location.href = '/music-collection/artists';
          } else {
            vm.errorMessage = response.data.mensagem;
            vm.btnSubmitText = 'login';
          }
        // error
        })
        .catch(function(error) {
          vm.btnSubmitText = 'login';
        });
    },

};
