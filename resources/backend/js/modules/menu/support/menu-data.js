export default {

  // artists is a section without childs, set as an empty array
  artists: [],
  albums: [],

  account: [

    {
      type: 'title',
      txt: 'My Account',
      icon: 'fa fa-user context-menu__title-icon',
    },

    {
      type: 'link',
      txt: 'Logout',
      link: '/logout',
    },

  ],

};
