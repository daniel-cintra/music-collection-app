export default {

  fetchAlbums(next) {
    axios.get('/api/album')
    .then((response) => {
      next((vm) => {
        vm.albums = response.data;
      });
    })
    .catch((error) => {
      next(false);
    });
  },

  deleteAlbum(vm, album) {
    axios.delete(`/api/album/${album.id}`)
    .then((response) => {
      if (response.data.status) {
        let index = vm.albums.indexOf(album);

        vm.albums.splice(index, 1);

        Vue.notify({
          type: 'success',
          text: 'Album deleted!',
        });
      }
    });
  },

};
