export default {

  storeArtist(params) {
    axios.post('/api/artist', params)
    .then((response) => {
      Vue.nextTick(() => {
        bus.$emit('artist/formSubmitted');
      });
    });
  },

  fetchArtist(to, next) {
    axios.get(`/api/artist/${to.params.id}/edit`)
    .then((response) => {
      next((vm) => {
        vm.id = response.data.id;
        vm.name = response.data.name;
        vm.twitter_handle = response.data.twitter_handle;
      });
    });
  },

  updateArtist(vm, params) {
    axios.put(`/api/artist/${vm.id}`, params)
    .then((response) => {
      bus.$emit('artist/formSubmitted');
    });
  },

};
