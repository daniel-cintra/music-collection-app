import * as types from './mutation-types';

export default {

  /**
   * Altera o contexto da interface do painel.
   *
   * @param state
   * @param json
   */
  [types.ATUALIZA_STATE](state, data) {
    state.nomeProjeto = data.nomeProjeto;
  },

};
