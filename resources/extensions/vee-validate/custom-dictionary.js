const customDictionary = {
  'pt_BR': {

    attributes: {
      user: 'usuário',
      password: 'senha',
    },

    messages: {
      cpf: () => 'Cpf inválido',
      cnpj: () => 'Cnpj inválido',
    },
    
  },
};

export default customDictionary;
