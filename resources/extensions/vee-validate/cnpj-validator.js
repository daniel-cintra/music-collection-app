import cnpjValidate from './cnpj-validate';

const validator = {
  getMessage(field, args) {
    return 'Invalid Cnpj';
  },
  validate(value, args) {
    return cnpjValidate(value);
  },
};

export default validator;
