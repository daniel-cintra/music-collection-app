import cpfValidate from './cpf-validate';

const validator = {
  getMessage(field, args) {
    return 'Invalid CPF';
  },
  validate(value, args) {
    return cpfValidate(value);
  },
};

export default validator;
