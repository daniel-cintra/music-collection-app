<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | The validator class. Some of the rules have multiple versions such
    | as the size rules. Feel free to tweak each of the messages.
    |
    */

    'accepted' => 'O campo :attribute deve ser aceito.',
    'active_url' => 'O campo :attribute não é uma URL válida.',
    'after' => 'O campo :attribute deve ser uma data posterior a :date.',
    'alpha' => 'O campo :attribute deve conter apenas letras.',
    'alpha_dash' => 'O campo :attribute deve conter apenas letras, números, and traços.',
    'alpha_num' => 'O campo :attribute deve conter apenas letras e números.',
    'before' => 'O campo :attribute deve ser uma data anterior a :date.',
    'between' => array(
        'numeric' => ':attribute deve ser um número entre :min - :max.',
        'file' => 'O campo :attribute deve estar entre :min - :max kilobytes.',
        'string' => 'O campo :attribute deve estar entre :min - :max caracteres.',
    ),
    'confirmed' => 'O campo de confirmação para :attribute está diferente do campo :attribute.',
    'date' => 'O campo :attribute não é uma data válida.',
    'date_format' => 'O campo :attribute não está no formato válido :format.',
    'different' => 'O campo :attribute e :other devem ser diferentes.',
    'digits' => 'O campo :attribute deve conter :digits dígitos.',
    'digits_between' => 'O campo :attribute deve estar entre :min e :max dígitos.',
    'email' => 'O :attribute digitado não é válido.',
    'exists' => 'O campo :attribute selecionado é inválido.',
    'image' => 'O :attribute deve ser um arquivo de imagem.',
    'in' => 'O campo :attribute selecionado é inválido.',
    'integer' => 'O campo :attribute deve ser um número.',
    'ip' => 'O campo :attribute deve ser um IP válido.',
    'max' => array(
        'numeric' => 'O campo :attribute não deve ser maior que :max.',
        'file' => 'O :attribute não pode ter mais de :max Kb.',
        'string' => 'O campo :attribute não pode ser maior que :max caracteres.',
    ),
    'mimes' => 'O :attribute deve estar no formato: :values.',
    'min' => array(
        // "numeric" => ":attribute deve ser ao menos :min.",
        'file' => 'O campo :attribute deve ter ao menos :min kilobytes.',
        'string' => 'O campo :attribute deve ter ao menos :min caracteres.',
    ),
    'not_in' => 'O campo :attribute selecionado é inválido.',
    'numeric' => 'O campo :attribute deve ser numérico.',
    'regex' => 'O formato do campo :attribute é inválido.',
    'required' => 'O campo :attribute é obrigatório.',
    'required_if' => 'O campo :attribute é obrigatório quando o campo :other é :value.',
    'required_with' => 'O campo :attribute é obrigatório quando :values está presente.',
    'required_without' => 'O campo :attribute é obrigatório quando o campo :values não é informado.',
    'same' => 'O campo :attribute e :other devem ser iguais.',
    'size' => array(
        'numeric' => 'O campo :attribute deve ter :size.',
        'file' => 'O campo :attribute deve ter :size kilobytes.',
        'string' => 'O campo :attribute deve ter :size characters.',
    ),
    'unique' => 'O :attribute digitado já consta como cadastrado.',
    'url' => 'O campo :attribute possui um formato inválido.',

    //custom rules
    'cpf' => 'O CPF digitado não é válido',
    'cnpj' => 'O CNPJ digitado não é válido',
    'creditcard' => 'O cartão de crédito digitado não é válido',
    'cep' => 'O :attribute informado não é válido',
    'is_url' => 'O :attribute informado não é válido',
    'email_unico_cliente' => 'O :attribute informado já está cadastrado',

    'recaptcha' => 'A verificação reCAPTCHA falhou. Tente novamente.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'password' => [
            'required' => 'O campo senha é obrigatório.',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [

        'nome_arquivo' => 'Arquivo de upload',
        'comentario' => 'comentário',
        'tipo_id' => 'Perfil do Produto',

        'slug_loja' => 'endereço da loja'

    ],

];
