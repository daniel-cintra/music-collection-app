<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "A senha deve ter no mínimo 6 caracteres e deve ser idêntica a confirmação de senha.",
	"user" => "O email digitado não existe em nosso cadastro.",
	"token" => "O token para alteração de senha é inválido.",
	"sent" => "Enviamos por email o link para alteração de senha.",
	"reset" => "Sua senha foi alterada com sucesso!",

];
