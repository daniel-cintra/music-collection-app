<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'A combinação de usuário e senha não é válida.',
    'throttle' => 'Excedido número de tentativas de login. Por favor tente novamente em :seconds segundos.',

];
