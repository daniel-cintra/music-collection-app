import VeeValidate from 'vee-validate';

import ptBr from 'vee-validate/dist/locale/pt_BR';
import customDictionary from 'resources/extensions/vee-validate/custom-dictionary';

import CpfValidator from 'resources/extensions/vee-validate/cpf-validator';
import CnpjValidator from 'resources/extensions/vee-validate/cnpj-validator';

VeeValidate.Validator.extend('cpf', CpfValidator);
VeeValidate.Validator.extend('cnpj', CnpjValidator);
VeeValidate.Validator.localize('pt_BR', ptBr);
VeeValidate.Validator.localize(customDictionary);

Vue.use(VeeValidate);
