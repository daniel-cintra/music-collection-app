<!DOCTYPE html>
<html lang="pt-br">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		{{-- usado nos requests axios --}}
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>@yield( 'meta-title', config('support.meta_title', 'Site Title') )</title>
		<meta name="description" content="@yield( 'meta-description' , config('support.meta_description', 'Site Description') )">

		@yield('metas')

		<link rel="stylesheet" href="{{ mix('/frontend/css/vendor.css') }}">
		<link rel="stylesheet" href="{{ mix('/frontend/css/app.css') }}">

		@yield('css')

		@stack('scriptsHead')

		{{-- @include('support::partials.ga') --}}

	</head>

	<body>

		<div id="app">

			@include('support::partials.top')

			@yield('content')

			@include('support::partials.footer')

		</div>

		<script src="{{ mix('/frontend/js/app.js') }}"></script>

		{{-- <script src="https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit&hl=pt-BR" async defer></script> --}}

		@stack('scriptsBody')

        {{-- <script src='https://www.google.com/recaptcha/api.js?hl=pt-BR'></script> --}}

{{-- 		@if( config('support.ga') )
             <script src="/frontend/js/ga.js"></script>
		@endif --}}

	</body>

</html>
