<footer class="footer-social mt-5 bg-secondary text-light d-flex justify-content-center">

    <section class="py-5">

        <a class="text-light py-2" href="https://twitter.com/planetoftheweb">
            <i class="layout-icon fab fa-twitter"></i>
        </a>

        <a class="text-light px-2" href="https://facebook.com/viewsource">
            <i class="layout-icon fab fa-facebook"></i>
        </a>

        <a class="text-light px-2" href="https://linkedin.com/in/planetoftheweb">
            <i class="layout-icon fab fa-linkedin"></i>
        </a>

        <a class="text-light px-2" href="https://github.com/planetoftheweb">
            <i class="layout-icon fab fa-github"></i>
        </a>
       
        <a class="text-light px-2" href="https://www.youtube.com/user/planetoftheweb">
            <i class="layout-icon fab fa-youtube"></i>
        </a>

    </section>

</footer>
