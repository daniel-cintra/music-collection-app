@if( $errors->any() )

	<div id="mensagens">

		{!! implode('', $errors->all('<div class="alert alert-danger">:message</div>')) !!}

	</div>

@elseif ( Session::has('message') )

	<div id="mensagens">

		<div class="alert alert-success">{{Session::get('message')}}</div>

	</div>

@endif