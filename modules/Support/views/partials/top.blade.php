<header>

    <nav class="family-sans text-uppercase navbar navbar-expand-md navbar-dark bg-dark">

        <div class="container-fluid">

            <a class="navbar-brand" href="/">
                <i class="fas fa-cube"></i> Layouts Components</a>

            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#myTogglerNav"
                aria-controls="#myTogglerNav" aria-label="Toggle Navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <section class="collapse navbar-collapse" id="myTogglerNav">
                <div class="navbar-nav ml-auto">
                    <a class="nav-item nav-link" href="/">home</a>
                    <a class="nav-item nav-link" href="/home/debug">debug</a>
                </div>
            </section>

        </div>

    </nav>

    <section class="top-hero d-flex align-items-center text-body text-center py-5">
        <div class="container">
            <h3>Bootstrap 4 Layouts</h3>
            <p>This layout uses multiple column rules so that how many columns each group takes depends on the width of
                the browser.
            </p>
        </div>
    </section>

</header>
