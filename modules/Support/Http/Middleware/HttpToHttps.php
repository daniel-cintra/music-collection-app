<?php

namespace App\Neo\Support\Http\Middleware;

use Closure;

class HttpToHttps
{
    public function handle($request, Closure $next)
    {
        if (!$request->secure() && config('app.env') === 'production') {
            $request->setTrustedProxies([$request->getClientIp()]);

            return redirect()->secure($request->getRequestUri(), 301);
        }

        return $next($request);
    }
}
