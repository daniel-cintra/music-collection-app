<?php

namespace Modules\Support;

use Illuminate\Support\Facades\Route;

class SupportServiceProvider extends BaseServiceProvider
{

    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Modules\Support\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     */
    public function boot()
    {
        // include __DIR__.'/helpers.php';
        // include __DIR__.'/Validators/recaptcha.php';

        $this->mergeConfigFrom(__DIR__.'/config/config.php', 'support');
        $this->loadViewsFrom(__DIR__.'/views', 'support');

        parent::boot();
    }
}
