<?php

namespace Modules\Support;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use ReflectionClass;

class BaseServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace;

    /**
     * Bootstrap services.
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     */
    public function map()
    {
        $this->mapWebRoutes();

        $this->mapApiRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     */
    protected function mapWebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {

            $routesPath = $this->getCurrentDir().'/routes/web.php';

            if (file_exists($routesPath)) {
                require $routesPath;
            }

        });
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     */
    protected function mapApiRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace' => $this->namespace,
            'prefix' => 'api',
        ], function ($router) {

            $routesPath = $this->getCurrentDir().'/routes/api.php';

            if (file_exists($routesPath)) {
                require $routesPath;
            }

        });
    }

    /**
     * Retorna o diretório do módulo em contexto.
     *
     * @return string
     */
    protected function getCurrentDir()
    {
        //conseguimos pegar sempre o diretório referente a classe filha e não a superclass
        $classInfo = new ReflectionClass($this);

        return dirname($classInfo->getFileName());
    }
}
