<?php

Route::get('/api/album', [
    'uses' => 'AlbumController@index',
]);

Route::post('/api/album', [
    'uses' => 'AlbumController@store',
]);

Route::get('/api/album/artists', [
    'uses' => 'AlbumController@create',
]);

Route::get('/api/album/{id}/edit', [
    'uses' => 'AlbumController@edit',
]);

Route::put('/api/album/{id}', [
    'uses' => 'AlbumController@update',
]);

Route::delete('/api/album/{id}', [
    'uses' => 'AlbumController@destroy',
]);