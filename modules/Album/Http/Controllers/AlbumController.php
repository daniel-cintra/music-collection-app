<?php

namespace Modules\Album\Http\Controllers;

use Modules\Support\Http\Controllers\BackendController;
use Modules\Album\Models\Album;
use Modules\Artist\Models\Artist;
use Modules\Album\Http\Requests\AlbumValidate;

/**
 * The responses in this Controller will be casted to Json Responses by Laravel...
 */
class AlbumController extends BackendController
{
    /**
     * List albums.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Album::with('artist')
        ->orderBy('name')
        ->get();
    }

    /**
     * List the artists to populate de UI artists combo.
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        return Artist::orderBy('name')->get(['id', 'name']);
    }

    /**
     * Save a new album.
     * 
     * @param \Modules\Album\Http\Requests\AlbumValidate
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AlbumValidate $request)
    {
        Album::create(
            request()->only('artist_id', 'name', 'year')
        );

        return ['status' => true];
    }

    /**
     * Get the album data to populate the initial state of the album form.
     * 
     * @param string
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id){

        $album = Album::findOrFail($id);
        
        $artists = Artist::orderBy('name')->get(['id', 'name']);

        return compact('album', 'artists');
    }

    /**
     * Update album.
     * 
     * @param \Modules\Album\Http\Requests\AlbumValidate
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(AlbumValidate $request, $id){
        return Album::where('id', $id)
        ->update(
            request()->only('artist_id', 'name', 'year')
        );
    }

    /**
     * Delete album.
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        Album::findOrFail($id)->delete();

        return ['status' => true];
    }
}
