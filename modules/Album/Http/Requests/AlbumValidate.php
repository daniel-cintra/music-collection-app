<?php

namespace Modules\Album\Http\Requests;

use Modules\Support\Http\Requests\Request;

class AlbumValidate extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'artist_id' => 'required|numeric',

            'name' => 'required',

            'year' => 'required|digits:4',

        ];

    }
}
