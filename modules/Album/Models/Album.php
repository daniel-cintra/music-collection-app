<?php

namespace Modules\Album\Models;

use Modules\Support\Models\BaseModel;

class Album extends BaseModel
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'albums';

    protected $guarded = [''];

    /**
     * Get the artist that recorded the album.
     */
    public function artist()
    {
        return $this->belongsTo('Modules\Artist\Models\Artist');
    }
}
