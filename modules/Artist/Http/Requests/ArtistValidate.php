<?php

namespace Modules\Artist\Http\Requests;

use Modules\Support\Http\Requests\Request;

class ArtistValidate extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'name' => 'required',

            'twitter_handle' => 'required',

        ];

    }
}
