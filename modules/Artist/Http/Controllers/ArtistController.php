<?php

namespace Modules\Artist\Http\Controllers;

use Modules\Support\Http\Controllers\BackendController;
use Modules\Artist\Models\Artist;
use Modules\Artist\Http\Requests\ArtistValidate;

/**
 * The responses in this Controller will be casted to Json Responses by Laravel...
 */
class ArtistController extends BackendController
{
    /**
     * List artists.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return Artist::orderBy('name')->get();
    }

    /**
     * Save a new artist.
     * 
     * @param \Modules\Artist\Http\Requests\ArtistValidate
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ArtistValidate $request)
    {
        Artist::create(
            request()->only('name', 'twitter_handle')
        );

        return ['status' => true];
    }

    /**
     * Get the artist data to populate the initial state of the artist form.
     * 
     * @param string
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id){
        return Artist::findOrFail($id);
    }

    /**
     * Update artist.
     * 
     * @param \Modules\Artist\Http\Requests\ArtistValidate
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ArtistValidate $request, $id){
        return Artist::where('id', $id)
        ->update(
            request()->only('name', 'twitter_handle')
        );
    }

    /**
     * Delete album.
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        Artist::findOrFail($id)->delete();

        return ['status' => true];
    }
}
