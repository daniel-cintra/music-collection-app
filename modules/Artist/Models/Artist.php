<?php

namespace Modules\Artist\Models;

use Modules\Support\Models\BaseModel;

class Artist extends BaseModel
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'artists';

    protected $guarded = [''];

    /**
     * Get the albums for the artist.
     */
    public function albums()
    {
        return $this->hasMany('Modules\Album\Models\Album');
    }
}
