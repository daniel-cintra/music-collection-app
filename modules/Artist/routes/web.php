<?php

Route::get('/api/artist', [
    'uses' => 'ArtistController@index',
]);

Route::post('/api/artist', [
    'uses' => 'ArtistController@store',
]);

Route::get('/api/artist/{id}/edit', [
    'uses' => 'ArtistController@edit',
]);

Route::put('/api/artist/{id}', [
    'uses' => 'ArtistController@update',
]);

Route::delete('/api/artist/{id}', [
    'uses' => 'ArtistController@destroy',
]);