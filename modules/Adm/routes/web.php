<?php

Route::group(['prefix' => 'music-collection'], function () {

    Route::get('/artists', [
        'uses' => 'AdmController@index',
    ]);

    // Vue catch all route
    Route::get('/{vue_capture?}', [
        'uses' => 'AdmController@index',
    ])->where('vue_capture', '[\/\w\.-]*');

 });
