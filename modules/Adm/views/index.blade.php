<!DOCTYPE html>
<html lang="pt-br">

	<head>

		<meta charset="utf-8">

		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>@yield( 'meta-title', 'Admin Panel' )</title>
		<meta name="description" content="Admin Panel">

		{{-- @include('painel.support::layouts.favicon') --}}

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0">

		<link rel="stylesheet" href="{{ mix('/backend/css/vendor.css') }}">
		<link rel="stylesheet" href="{{ mix('/backend/css/app.css') }}">

	</head>

	<body>

		<div id="app">

			<master></master>

		</div>

		<script src="{{ mix('/backend/js/app.js') }}"></script>

	</body>

</html>
