<?php

namespace Modules\Adm;

use Modules\Support\BaseServiceProvider;

class AdmServiceProvider extends BaseServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Modules\Adm\Http\Controllers';

    /**
     * Bootstrap the application events.
     */
    public function boot()
    {
        parent::boot();

        $this->loadViewsFrom(__DIR__.'/views', 'adm');
    }
}
