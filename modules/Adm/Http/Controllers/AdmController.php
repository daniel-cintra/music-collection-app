<?php

namespace Modules\Adm\Http\Controllers;

use Modules\Support\Http\Controllers\BackendController;

class AdmController extends BackendController
{
    
    /**
     * App entry point.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('adm::index');
    }
    
}
