<?php

namespace Modules\Login;

use Modules\Support\BaseServiceProvider;

class LoginServiceProvider extends BaseServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Modules\Login\Http\Controllers';

    /**
     * Bootstrap the application events.
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/views', 'login');

        parent::boot();
    }
}
