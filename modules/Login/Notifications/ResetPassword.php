<?php

namespace Modules\Login\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

use Modules\Login\Models\Usuario;

class ResetPassword extends Notification
{
    use Queueable;

    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Create a notification instance.
     *
     * @param string $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail()
    {
        return (new MailMessage())
            ->view('support::emails.padrao')
            ->line([
                'Você está recebendo este email porque nós recebemos
            uma solicitação de reset de senha da sua conta.',
                'Clique no botão abaixo para resetar sua senha:',
            ])
            ->action('Resetar Senha', url('painel/password/reset', [$this->token]))
            ->line('Se você não solicitou o reset da senha, pode desconsiderar
            esta mensagem.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
