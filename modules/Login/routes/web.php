<?php

Route::get('/', [
    'uses' => 'LoginController@showLoginForm',
]);

Route::post('panel/login', [
    'as' => 'panel.login',
    'uses' => 'LoginController@login',
]);

Route::get('panel/logout', [
    'uses' => 'LoginController@logout',
]);