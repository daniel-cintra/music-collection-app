<!DOCTYPE html>
<html lang="pt-br">

	<head>
		
		<meta charset="utf-8">
		
		<title>Adm Login</title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		{{-- used in axios requests --}}
		<meta name="csrf-token" content="{{ csrf_token() }}">
		
		<link rel="stylesheet" href="/backend/css/vendor.css">
		<link rel="stylesheet" href="/backend/css/app.css">
		
	</head>

	<body class="login__body">
		
		<div class="login__container">
			
			<div id="app" class="login__box">
				
				<login-form></login-form>
				
				<p class="mt-5 mb-3 text-muted">© Copyright {{date('Y')}}</p>
				
			</div>
			
		</div>

		<script src="{{ mix('/backend/js/app.js') }}"></script>
		
	</body>

</html>
